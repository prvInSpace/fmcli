##

DESTDIR = /usr/local/bin

install: setup
	@echo "Installing fmcli.."
	ln -s -f $(shell pwd)/fmcli.py $(DESTDIR)/fmcli

setup:
	@read -p "Enter your Last.fm API key: " KEY; \
	echo $$KEY | awk '{print "api_key = \""$$1"\""}' > key.py;
	@read -p "Enter Last.fm username: " USR; \
	echo $$USR | awk '{print "default_user = \""$$1"\""}' >> key.py;

link: 
	ln -s -f $(shell pwd)/fmcli.py $(DESTDIR)/fmcli
	

uninstall:
	@echo "Uninstalling.."
	rm $(DESTDIR)/fmcli
