
from enum import Enum
import json
import pandas as pd

class Language(Enum):
    XX = "Unknown"
    EN = "English"
    BR = "Breton"
    DE = "German"
    ES = "Spanish"
    FR = "French"
    IS = "Icelandic"
    IT = "Italian"
    NO = "Norwegian"
    NL = "Dutch"
    CY = "Welsh"
    SV = "Swedish"
    UK = "Ukrainian"
    INST = "Instrumental"

__language_data = None
__artist_warning_given = list()

def __get_data() -> dict[str, dict]:
    global __language_data
    if not __language_data:
        with open(f"{__file__.replace('/languages.py', '')}/lang.json") as input:
            __language_data = json.load(input)
    return __language_data

def __convert_str_to_lang(s: str) -> Language:
        s = s.upper()
        if s in Language.__members__:
            return Language[s]
        raise Exception(f"Unknown language {s}")

def __read_language_list(obj, key) -> list[Language]:
    value = obj[key]
    if type(value) == str:
        value = [ value ]
    return map(
        lambda x: __convert_str_to_lang(x),
        value
    )

def get_language(artist: str, album: str = None, song: str = None) -> list[Language]:
    data = __get_data()
    global __artist_warning_given
    if artist not in data:
        if artist not in __artist_warning_given:
            print(f"Warning: No data for artist {artist}")
            __artist_warning_given.append(artist)
        return [ Language.XX ]

    artist_data = data[artist]

    if "songs" in artist_data:
        songs = artist_data["songs"]
        if song in songs:
            return __read_language_list(songs, song)

    if "albums" in artist_data:
        albums = artist_data["albums"]
        if album in albums:
            return __read_language_list(albums, album)

    if "default_lang" not in artist_data:
        print(f"Warning: No default language for artist {artist}")
        return [ Language.XX ]

    return __read_language_list(artist_data, "default_lang")
    

def songs_by_language(songs: pd.DataFrame) -> pd.DataFrame:
    # Requires "Artist", "Song", and Count to be present
    languages = {}
    songs['Count'] = songs['Count'].astype(int)
    for _, row in songs.iterrows():
        langs = get_language(row.Artist, song=row.Song)
        for lang in langs:
            languages.setdefault(lang, 0)
            languages[lang] += row.Count

    df = pd.DataFrame(
        languages.items(),
        columns=['Language', 'Count']
    )
    df.sort_values(by="Count", ascending=False, ignore_index=True, inplace=True)
    df['Language'] = df['Language'].transform(
        lambda x: x.value
    )
    df['Count'] = df['Count'].astype(str)
    return df 


def add_languages_to_dataframe(data: pd.DataFrame):
    """Returns a new DataFrame with a Language column
    containing the languages of each row.
    """
    data["Language"] = data.apply(
        lambda row:
            ", ".join(map(
                lambda x: x.value,
                __get_lang_by_row(row)
            )),
        axis=1
    )
    return data


def __get_lang_by_row(row) -> list[Language]:
    """Helper function to add_languages_to_dataframe.
    Converts a row in to a proper function call to
    get_language.
    """
    artist = row['Artist'] if "Artist" in row else None
    album = row['Album'] if "Album" in row else None
    song = row['Song'] if "Song" in row else None
    return get_language(artist, album, song)
